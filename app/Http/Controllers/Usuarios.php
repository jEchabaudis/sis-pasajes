<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Usuarios extends Controller
{
    public function acceso(){
        return view("login");
    }

    public function acceder(Request $request){
        $this->validate($request, [
            'usuario' => 'required',
            'contrasena' => 'required|min:5',
        ]);

        $usuario = DB::select("select top 1 * from Usuarios where usuario=:usuario and contraseña=:contrasena",["usuario"=>$request->usuario,"contrasena"=>$request->contrasena]);

        if (count($usuario)==0) {
            return \Redirect::back()->withErrors(['msg' => 'El nombre de usuario o la contraseña que ingresaste son incorrectos. Por favor, revisa e inténtalo de nuevo.']);
        }

        session(["login"=>$usuario[0]]);

        return redirect('/perfil');
    }

    public function perfil(){
        return view("perfil");
    }

    public function cerrarSesion(){
        session()->flush();
        return redirect('/');
    }

    public function registro(){
        return view("registro");
    }

    public function grabRegistro(Request $request){
        $this->validate($request, [
            'usuario' => 'required',
            'dni' => 'required|digits:8',
            'nombres' => 'required',
            'apellidos' => 'required',
            'email' => 'required|email',
            'contrasena' => 'required|min:5',
            'rep_contrasena' => 'required|min:5',
        ]);
        
        if ($request->contrasena!=$request->rep_contrasena) {
            return \Redirect::back()->withErrors(['msg' => 'El campo contraseña no coincide con el campo repita contraseña']);
        }

        DB::insert("INSERT INTO Usuarios(usuario,dni,nombres,apellidos,email,contraseña) VALUES(:usuario,:dni,:nombres,:apellidos,:email,:contrasena)",["usuario"=>$request->usuario,"dni"=>$request->dni,"nombres"=>$request->nombres,"apellidos"=>$request->apellidos,"email"=>$request->email,"contrasena"=>$request->contrasena]);

        return redirect('/');
    }

    public function modificacion(){
        $usuario = DB::select("select top 1 * from Usuarios where usuario=:usuario",["usuario"=>session("login")->usuario]);
        return view("modificacion",["usuario"=>$usuario[0]]);
    }

    public function grabModificacion(Request $request){
        $this->validate($request, [
            'dni' => 'required|digits:8',
            'nombres' => 'required',
            'apellidos' => 'required',
            'email' => 'required|email',
        ]);

        DB::update("UPDATE Usuarios SET dni=:dni,nombres=:nombres,apellidos=:apellidos,email=:email,fechaMod=GETDATE() WHERE usuario=:usuario",["usuario"=>session("login")->usuario,"dni"=>$request->dni,"nombres"=>$request->nombres,"apellidos"=>$request->apellidos,"email"=>$request->email]);

        return redirect()->back()->with('mensaje', 'Los datos se modificaron correctamente.');
    }

    public function baja(){
        return view("baja");
    }

    public function grabBaja(){
        DB::delete("delete from Usuarios where usuario=:usuario",["usuario"=>session("login")->usuario]);
        session()->flush();
        return redirect('/baja-exitoso');
    }

    public function bajaExitos(){
        return view("baja-exitoso");
    }
}
