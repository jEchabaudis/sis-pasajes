<?php

namespace App\Http\Middleware;

use Closure;

class Invitado
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session("login")){
            if ($request->ajax())
            {
                return response(["login"=>true], 200);
            }
            return redirect('/perfil');
        }

        return $next($request);
    }
}
