<?php

Route::get('/','Usuarios@acceso')->middleware('invitado');
Route::post('/acceder','Usuarios@acceder')->middleware('invitado');
Route::get('/registro','Usuarios@registro')->middleware('invitado');
Route::post('/grabRegistro','Usuarios@grabRegistro')->middleware('invitado');
Route::get('/baja-exitoso','Usuarios@bajaExitos')->middleware('invitado');

Route::group(['prefix' => '/','middleware'=>['autenticado']], function(){
    Route::get('/perfil','Usuarios@perfil');
    Route::get('/cerrarSesion','Usuarios@cerrarSesion');
    Route::get('/modificacion','Usuarios@modificacion');
    Route::post('/grabModificacion','Usuarios@grabModificacion');
    Route::get('/baja','Usuarios@baja');
    Route::post('/grabBaja','Usuarios@grabBaja');
});