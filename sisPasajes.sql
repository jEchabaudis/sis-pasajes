create database sisPasajes
go
use sisPasajes
go

create table Usuarios(
	usuario varchar(50) not null,
	dni char(8) not null,
	nombres varchar(100) not null,
	apellidos varchar(100) not null,
	email varchar(50) not null,
	contraseña varchar(255) not null,
	fechaReg datetime default(GETDATE()),
	fechaMod datetime default(GETDATE()),
	CONSTRAINT PK_Usuarios PRIMARY KEY (usuario)
)
GO

