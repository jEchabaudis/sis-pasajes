@extends('template')

@section('content')
<div class="row justify-content-center mb-5">
	<div class="col-5">
		<div class="alert alert-success text-center">
			<h2 class="text-center mb-4">Usted se ha dado de baja del sistema correctamente.</h2>
			<a href="/">Ir a Inicio</a>
		</div>
	</div>
</div>
@endsection