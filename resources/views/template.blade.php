<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sistema de Ventas de Pasaje - Expreso Molina Union SAC</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		.selector-for-some-widget {
		  box-sizing: content-box;
		}
	</style>
</head>
<body style="background: #336501;">
	<div class="container-fluid py-3 bg-white mt-5">
		<div class="row justify-content-between align-items-center">
			<div class="col-auto">
				@if(session("login"))
					<div>{{session("login")->usuario}}</div>
					<a href="/cerrarSesion">Cerrar Sesión</a>
				@endif
			</div>
			<div class="col-auto">
				<img src="/img/expreso-molina.jpg">
			</div>
		</div>
	</div>
	<div class="container-fluid mt-5 mb-5">
		@yield('content')
	</div>
</body>
</html>