@extends('template')

@section('content')
<div class="row justify-content-center">
	<div class="col-5">
		<form action="grabBaja" class="border rounded p-5" style="background: #b4d4a5;" method="POST">
			<h4 class="text-center">Usted se dará de baja del sistema, ¿Desea continuar?</h4>
			<div>
				<div class="text-center">
					<button class="btn btn-primary">Si</button>
					<a href="/perfil" class="btn btn-light">No</a>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection