@extends('template')

@section('content')
<div class="row justify-content-center mb-5">
	<div class="col-5">
		<h2 class="text-center mb-4">Bienvenido {{session("login")->usuario}}</h2>
	</div>
</div>
<div class="row justify-content-center">
	<div class="col-auto">
		<ul class="list-inline">
			<li class="list-inline-item"><a href="/modificacion" class="text-light">Modificación de datos</a></li>
			<li class="list-inline-item"><a href="/baja" class="text-light">Dar de Baja</a></li>
		</ul>
	</div>
</div>
@endsection