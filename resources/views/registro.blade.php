@extends('template')

@section('content')
<div class="row justify-content-center">
	<div class="col-5">
		<h4 class="text-center mb-4">Ingresar datos para registro</h4>
		<form action="/grabRegistro" class="border rounded" method="POST">
			<div class="p-4">
				<div class="form row">
					@if($errors->any())
						<div class="mb-3 alert alert-danger">
						   @foreach ($errors->all() as $error)
						      <div>{{ $error }}</div>
						  @endforeach
						</div>
					@endif
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Usuario:</label>
							<div class="col-8">
								<input type="text" value="{{ old('usuario') }}" class="form-control mb-3" name="usuario" autocomplete="off" maxlength="50" autofocus>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">DNI:</label>
							<div class="col-8">
								<input type="text" value="{{ old('dni') }}" class="form-control mb-3" name="dni" autocomplete="off" maxlength="8">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Nombres:</label>
							<div class="col-8">
								<input type="text" value="{{ old('nombres') }}" class="form-control mb-3" name="nombres" autocomplete="off" maxlength="100">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Apellidos:</label>
							<div class="col-8">
								<input type="text" value="{{ old('apellidos') }}" class="form-control mb-3" name="apellidos" autocomplete="off" maxlength="100">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Email:</label>
							<div class="col-8">
								<input type="text" value="{{ old('email') }}" class="form-control mb-3" name="email" autocomplete="off" maxlength="50">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Contraseña:</label>
							<div class="col-8">
								<input type="password" class="form-control mb-3" name="contrasena" maxlength="255">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Repita Contraseña:</label>
							<div class="col-8">
								<input type="password" class="form-control mb-3" name="rep_contrasena" maxlength="255">
							</div>
						</div>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-primary">Grabar</button>
					<a href="/" class="btn btn-link text-light">Cancelar</a>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection