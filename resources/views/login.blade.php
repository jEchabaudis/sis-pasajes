@extends('template')

@section('content')
<div class="row">
	<div class="col-6 text-center">
		<img src="/img/bus.png" width="400px">
	</div>
	<div class="offset-1 col-4">
		<form action="/acceder" class="border rounded" method="POST">
			<h4 class="bg-danger px-3 py-2 text-light">Login</h4>
			<div class="p-5">
				<div class="form row">
					@if($errors->any())
						<div class="mb-3 alert alert-danger">
						   @foreach ($errors->all() as $error)
						      <div>{{ $error }}</div>
						  @endforeach
						</div>
					@endif
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Usuario:</label>
							<div class="col-8">
								<input type="text" class="form-control mb-3" name="usuario" autocomplete="off" maxlength="50" autofocus>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Contraseña:</label>
							<div class="col-8">
								<input type="password" class="form-control mb-3" name="contrasena" maxlength="255">
							</div>
						</div>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-primary">Ingresar</button>
					<a href="/registro" class="btn btn-link text-light">Registrarme</a>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection