@extends('template')

@section('content')
<div class="row justify-content-center">
	<div class="col-5">
		<h4 class="text-center mb-4">Modifiación de datos</h4>
		<form action="/grabModificacion" class="border rounded" method="POST">
			<div class="p-4">
				<div class="form row">
					@if($errors->any())
						<div class="mb-3 alert alert-danger">
						   @foreach ($errors->all() as $error)
						      <div>{{ $error }}</div>
						  @endforeach
						</div>
					@endif
					@if(session()->has('mensaje'))
					    <div class="alert alert-success">
					        {{ session()->get('mensaje') }}
					    </div>
					@endif
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Usuario:</label>
							<div class="col-8">
								<input type="text" value="{{ old('usuario',$usuario->usuario) }}" class="form-control mb-3" name="usuario" autocomplete="off" disabled>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">DNI:</label>
							<div class="col-8">
								<input type="text" value="{{ old('dni',$usuario->dni) }}" class="form-control mb-3" name="dni" maxlength="8" autocomplete="off">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Nombres:</label>
							<div class="col-8">
								<input type="text" value="{{ old('nombres',$usuario->nombres) }}" class="form-control mb-3" name="nombres" maxlength="100" autocomplete="off">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Apellidos:</label>
							<div class="col-8">
								<input type="text" value="{{ old('apellidos',$usuario->apellidos) }}" class="form-control mb-3" name="apellidos" maxlength="100" autocomplete="off">
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row no-gutters">
							<label class="col-4 col-form-label pr-1 text-end text-light">Email:</label>
							<div class="col-8">
								<input type="text" value="{{ old('email',$usuario->email) }}" class="form-control mb-3" name="email" maxlength="50" autocomplete="off">
							</div>
						</div>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-primary">Grabar</button>
					<a href="/perfil" class="btn btn-outline-light">Cancelar</a>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection